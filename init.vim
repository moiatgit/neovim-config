set nocompatible              " be iMproved, required

" Spelling
highlight clear SpellBad
highlight SpellBad term=standout ctermfg=1 term=underline cterm=underline
highlight clear SpellCap
highlight SpellCap term=underline cterm=underline
highlight clear SpellRare
highlight SpellRare term=underline cterm=underline
highlight clear SpellLocal
highlight SpellLocal term=underline cterm=underline

set hlsearch " highlight search

" change highlight colors for search to ease distinction
" highligh Search guibg=DarkYellow guifg=DarkGreen
" highligh IncSearch guibg=DarkBlue guifg=Yellow
" highligh Cursor guibg=Yellow guifg=DarkBlue

" coding
syntax on " syntax highlight
filetype plugin on " plugin detection
set autochdir " always switch to the current file directory
set number " line numbers
set hlsearch " highlight search
set incsearch " show search matches while typing
set showmatch " show matching elements
set ignorecase " case unsensitive search
set smartcase " if there are caps go case-sensitive
set history=2000 " history length
set showcmd " show mode
set hidden  " allows edited buffers not visible in any window
set list " show hidden chars
"set listchars=tab:^T,eol:· " hidden chars representation
set listchars=tab:→\ ,trail:·,extends:>,eol:¶" hidden chars representation
" status line
set statusline =
set statusline +=%{fugitive#statusline()}
set statusline +=\ %F%m%h%w\ [%p%%]
set statusline +=[%{winnr()}/%n]   " split nr / buffer nr
set statusline +=\ %{''!=#&filetype?&filetype:'none'}    " filetype
set statusline +=%=%-14.(%l,%c%V%)\ %P
set laststatus=2    " show the status line always

set omnifunc=on " autocomplete function
"set completeopt=menu,preview " autocomplete function
set completeopt=menu,menuone,preview,noselect,noinsert " autocomplete function
set wildmenu " command-line completion
set scrolloff=3 " lines before EOF
"
let &showbreak = '↳ '
set wrap
set cpo=n
set breakindent
"
set autoindent " code autoindent
set smartindent " advanced indent
set backspace=indent,eol,start
set tabstop=4 " tab with
set shiftwidth=4 " tabs
set softtabstop=4 " tabs
set expandtab " don't use real tabs
set textwidth=74 " default width

set cursorline    " highlight current line
" backups
set backup " backup files
set backupdir=~/tmp/vitmp,. " backup files
set directory=~/tmp/vitmp,. " swap files
" mappings
" up/down keys move one line not paragraph
map <Up> gk
map <Down> gj
" Mapping for continued indentation on visual
vnoremap > >gv
vnoremap < <gv

" reload when source changes
checktime
set autoread
" allow ctrl-shif selection
behave mswin
"
" share clipboard with system's
set clipboard=unnamedplus
" Remove some gui options
set guioptions-=T " Elimina la toolbar
set guioptions-=m " remove the menu bar
" Remove scrollbars: useful for tiling wm as Awesome WM.
set guioptions-=L " remove left scrollbars: on splitted windows
set guioptions-=l " remove left scrollbar
set guioptions-=R " remove right scrollbar on
set guioptions-=r " remove right scrollbar
set guioptions-=b " remove bottom scrollbar
set guioptions-=h " limit the scrolling to the current cursor line
"
" Enable mouse
set mouse=a
"
" Afegeix un $ al final del text afectat per una comanda (c)hange
set cpoptions+=$
"
" càrrega de templates (si existeixen)
autocmd BufNewFile * silent! 0r ~/.vim/templates/%:e.tpl

" for rst file
autocmd BufEnter *.rst setlocal spell spelllang=ca
autocmd BufEnter *.rst highlight clear SpellBad
autocmd BufEnter *.rst highlight SpellBad ctermfg=3 term=underline cterm=underline
"
" disable riv auto folding behavior
let g:riv_disable_folding = 1

"
" redefine leader key
let mapleader = "¡"

" Assign quiz extension to rst
autocmd BufRead,BufNewFile *.quiz set filetype=rst

" special indentation for python files
autocmd BufEnter *.py set cindent
autocmd BufEnter *.py set textwidth=0

" Special indentation for XML files
autocmd Filetype xml set softtabstop=2 shiftwidth=2 expandtab
autocmd Filetype xsd set softtabstop=2 shiftwidth=2 expandtab

" Split to the bottom and the right
set splitbelow
set splitright

" Mappings for split navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Mapping for file-under-cursor creation
nmap <leader>gf :tabe <cfile><cr>

" Mapping for fast :nohlsearch
nmap <leader>h :noh<CR>
imap <leader>h :noh<CR>

" Mapping for python formating
" separate functions
nmap <leader>pd :%s/\\n\\+\\(\\s*def \\)/\\r\\r\\r\\1<CR>:noh<CR><C-o>

" Different formating maps
"
" Mapping for quiz formating
"nmap <leader>fq :%s/\\n\\+\\ze\\.\\. pregunta:/\\r\\r\\r\\r<CR>:%s/\\n\\+\\.\\.\\s\\+\\ze\\(enunciat\\\|resposta\\)/\\r\\r.. <CR>:noh<CR><C-o>

" Clean up leading whitespace
nmap <leader>fs :%s/\\s\\+$/<CR>:noh<CR>

" For encryption
" XXX find out how to make this work in neovim
"autocmd VimEnter * if ! empty(&l:key) | set viminfo='0,\"0,\/0,:0,f0 | echomsg "Adapted for encrypted editing" | endif
"setlocal cryptmethod=blowfish2

" Search down into subfolders: tab completioin for all file related tasks
set path+=*

" Keep in undo each paragraph
inoremap <CR> <CR><c-g>u

" Two leaders to esc
ino <leader><leader> <Esc>

" File autocompletion
set wildmode=longest,list,full
set wildmenu

" Do not fix EOF: do not add an automatic EOL at the end of the file
set nofixendofline

" Terminal emulator
:tnoremap <ESC> <C-\><C-n>

" Python environment
let g:python3_host_prog = '/usr/bin/python3'


" Configuration for plug
call plug#begin()
Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}  " semantic syntax highlighting for python
Plug 'scrooloose/nerdtree'                              " file navigation tree
Plug 'Xuyuanp/nerdtree-git-plugin'                      " nerdtree git aware
Plug 'moiatgit/vim-rst-sections'
Plug 'machakann/vim-sandwich'                           " manages surroundings like () []…
Plug 'godlygeek/tabular'                                " tabularize
Plug 'neomake/neomake'                                  " checks for syntax errors
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }   " Languages auto-completion
Plug 'deoplete-plugins/deoplete-jedi'                   " deoplete for python
Plug 'artur-shaik/vim-javacomplete2'                    " java completion
Plug 'dense-analysis/ale'                               " linter for various languages
Plug 'mhinz/vim-signify'                                " git differences
Plug 'tpope/vim-fugitive'                               " git wrapper
Plug 'morhetz/gruvbox'                                  " colorschema
Plug 'wellle/targets.vim'                               " adds new text objects
Plug 'rust-lang/rust.vim'                               " Rust syntax highlighting
Plug 'Yggdroot/indentLine'                              " Vertical indentation
Plug 'szw/vim-maximizer'                                " Window maximizer
Plug 'itchyny/lightline.vim'
Plug 'scrooloose/nerdcommenter'                         " comment: https://github.com/preservim/nerdcommenter#default-mappings
Plug 'davidhalter/jedi-vim'                             " python jump definition
"
"Plug 'Chiel92/vim-autoformat'                           " python format
"Plug 'Vimjas/vim-python-pep8-indent'                    " python indentation
Plug 'sbdchd/neoformat'                                 " code formating

call plug#end()

" Configuration for semshi
let g:semshi#filetypes                      = ['python']
let g:semshi#excluded_hl_groups             = ['local']
let g:semshi#mark_selected_nodes            = 2
let g:semshi#no_default_builtin_highlight   = v:true
let g:semshi#simplify_markup                = v:true
let g:semshi#error_sign                     = v:false
let g:semshi#error_sign_delay               = 1
let g:semshi#always_update_all_highlights   = v:true
let g:semshi#tolerate_syntax_errors         = v:true
let g:semshi#update_delay_factor            = 0.0001
let g:semshi#self_to_attribute              = v:true
function MyCustomHighlights()
    hi semshiSelected ctermbg = 243
endfunction
autocmd FileType python call MyCustomHighlights()
autocmd ColorScheme * call MyCustomHighlights()

" Configure Neomake
" Full config: when writing or reading a buffer, and on changes in insert and
" normal mode (after 1s; no delay when writing).
call neomake#configure#automake('nrwi', 500)
let g:neomake_python_enabled_makers = ['flake8', 'pylint']"
"let g:neomake_open_list = 2

" Configure nerdtree
map <leader>t :NERDTreeToggle<CR>
"let g:NERDTreeIndicatorMapCustom = {
let g:NERDTreeGitStaturIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Deoplete configuration
" Following https://averywagar.com/posts/2018/01/configuring-vim-for-java-development/
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:deoplete#enable_at_startup = 1

call deoplete#custom#option({
    \ 'auto_complete_delay': 100,
    \ 'smart_case': v:true,
    \ })
"
" completion sources
call deoplete#custom#option('sources', {
      \ '_': ['buffer'],
      \ 'java': ['jc', 'javacomplete2', 'file', 'buffer'],
      \ })

" auto close method preview window
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" javacomplete2 configuration
" ===============================================================

" add java completion when opening a java file
autocmd FileType java setlocal omnifunc=javacomplete#Complete
autocmd FileType java JCEnable
"
" ale configuration
" ===============================================================

" Shorten error/warning flags
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
" I have some custom icons for errors and warnings but feel free to change them.
let g:ale_sign_error = '✘✘'
let g:ale_sign_warning = '⚠⚠'

" Disable or enable loclist at the bottom of vim
" Comes down to personal preferance.
let g:ale_open_list = 0
let g:ale_loclist = 0


" Setup compilers for languages

let g:ale_linters = {
      \  'cs':['syntax', 'semantic', 'issues'],
      \  'python': ['flak8', 'pylint', 'mypy'],
      \  'java': ['javac'],
      \  'reStructuredText': ['rstcheck'],
      \  'bash': ['shellcheck']
      \ }

" set auto-completion
let g:ale_completion_enabled = 1

" Go-To definition
nnoremap <C-LeftMouse> :ALEGoToDefinition<CR>

" Configure fixers
let g:ale_fixers = { 'rust': ['rustfmt', 'trim_whitespace', 'remove_trailing_lines'] }


" ===============================================================
" Colorscheme
" ===============================================================

" gruvbox
" ===============================================================
let g:gruvbox_contrast_dark = 'hard'
let g:gruvbox_improved_strings = 0
let g:gruvbox_improved_warnings = 1
colorscheme gruvbox


" itchyny
" ===============================================================
set noshowmode          " with itchyny status bar it is not necessary to show
                        " the mode
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead',
      \   'folder': 'getcwd',
      \ },
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'folder', 'readonly', 'filename', 'modified' ] ],
      \   'right': [ [ 'lineinfo' ],
      \              [ 'percent' ],
      \              [ 'filetype', 'charvaluehex' ] ]
      \ },
      \ }

" ===============================================================
" Abbreviations
" ===============================================================
"java
"autocmd FileType java :iabbrev pclass public class <C-R>=expand("%:r")<CR><ESC>a {<CR>}<ESC>O <ESC>hC
"autocmd FileType java :iabbrev pmain public static void main(String[] args) {<CR>}<ESC>O <ESC>hC
"autocmd FileType java :iabbrev sout System.out.println("");<ESC>3hi<C-O>
"autocmd FileType java :iabbrev souf System.out.printf("%n");<ESC>5hi<C-O>

autocmd FileType java :iabbrev pclass public class <c-r>=expand("%:r")<cr> {<cr><cr>}<esc>k3a <esc>a
autocmd FileType java :iabbrev pinterface public interface <c-r>=expand("%:r")<cr> {<cr><cr>}<esc>k3a <esc>a
autocmd FileType java :iabbrev pmain public static void main(String[] args){<cr><cr>}<esc>k7a <esc>a
autocmd FileType java :iabbrev sout System.out.println("");<esc>3hi<c-o>
autocmd FileType java :iabbrev souf System.out.printf("%n");<esc>5hi<c-o>

" Maximizer Toggle
nnoremap <silent><F3> :MaximizerToggle<CR>
vnoremap <silent><F3> :MaximizerToggle<CR>gv
inoremap <silent><F3> <C-o>:MaximizerToggle<CR>

" ela geminada
imap ·l ŀl

" vim-autoformat
let g:python3_host_prog="/usr/bin/python3"
noremap <F3> :Autoformat<CR>
let g:autoformat_autoindent = 0
let g:autoformat_retab = 0
let g:autoformat_remove_trailing_spaces = 0

" jedi-vim
" disable autocompletion, because we use deoplete for completion
let g:jedi#completions_enabled = 0
" open the go-to function in split, not another buffer
let g:jedi#use_splits_not_buffers = "right"
